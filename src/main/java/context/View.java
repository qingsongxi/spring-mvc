package context;

import lombok.Data;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 类 名: View
 * 描 述: 视图文件
 */
@Data
public class View {
    /**
     * 视图名称mvc.jsp
     */
    private String  viewName;
    /**
     * 视图文件
     */
    private File file;

    public View(String viewName, File file) {
        this.viewName = viewName;
        this.file = file;
    }

    /**
     * 正则表达式匹配 ${} 标签
     */
    Pattern pattern=Pattern.compile("[.\\w]*[${]([\\w]+)[}]");

    /**
     * 解析视图文件，替换视图文件中的占位符
     * @param modelAndView
     * @param response
     */
    public void replaceAllEl(ModelAndView modelAndView, HttpServletResponse response) throws IOException {
        //读取flie内容
        BufferedReader reader=new BufferedReader(new FileReader(file));
        //替换完成结果
        StringBuffer result=new StringBuffer();
        while (reader.read()>0){
            String line=reader.readLine();
            //使用正则表达式替换掉 ${}内容
            Matcher matcher=pattern.matcher(line);
            //有匹配的表达式
            while (matcher.find()) {
                //拿到表达式名称
                String key = matcher.group(1);
                //从返回结果中找到与表达式匹配的变量,并替换
                if (modelAndView.getModel().containsKey(key)) {
                    //替换对应的内容
                    line = line.replace("${" + key + "}", modelAndView.getModel().get(key).toString());
                }
            }
            // 将替换后的 或者没替换的  进行新写入
            result.append(line);
        }
        // 将数据写会响应
        response.getWriter().write(result.toString());
    }
}
