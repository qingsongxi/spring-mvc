package context;

import lombok.Data;

import java.util.HashMap;

/**
 * 类 名: ModleAndView
 * 描 述: 视图及相应参数
 */
@Data
public class ModelAndView {
    /**
     * 视图名称
     */
    private Object viewData;
    /**
     * 响应参数
     */
    private HashMap model = new HashMap();
    /**
     * 是否为一个视图对象
     */
    private boolean hasView = false;

    public void addObject(Object key, Object value){
        this.model.put(key, value);
    }

}
