package context;

import core.BeanDefinition;
import core.annotation.MyController;
import core.annotation.MyRequestMapping;
import factory.DefaultListableBeanFactory;
import handler.HandlerMappers;
import handler.ParameterMapper;
import org.apache.commons.lang3.StringUtils;
import utils.MethodHelper;
import utils.ParameterTypeUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 类 名: DispatcherServlet
 *
 * @author 52874
 */
@WebServlet(name = "dispatcherServlet")
public class DispatcherServlet extends HttpServlet {

    private AnnotationConfigApplicationContext context;
    private DefaultListableBeanFactory beanFactory;
    /**
     * 请求url与访问控制层的映射关系
     */
    private ConcurrentHashMap<String, HandlerMappers> handlerMappersMap = new ConcurrentHashMap<>();
    /**
     * 存储项目中的页面信息
     */
    private  ArrayList<View> viewList=new ArrayList<>();

    @Override
    public void init() throws ServletException {
        /**
         * 初始化容器
         */
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.jiayao");
        this.context = context;
        this.beanFactory = context.beanFactory;
        /**
         * 获取所有的Controller类的映射关系
         */
        getTargetController();
//        /**
//         * 获取所有的方法参数关系
//         */
        getTargetProperties();
        /**
         * 初始化视图信息
         */
        initViewResolver();
    }

    private void initViewResolver() {
        try {
            String path = beanFactory.getProperties("view.rootPath");
            //遍历文件夹获取到所有jsp文件
            URL url = this.getClass().getClassLoader().getResource(path);
            //拿到文件夹
            assert url != null;
            File file = new File(url.toURI());
            doLoadFile(file);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void doLoadFile(File file) {
        String suffix=beanFactory.getProperties("view.suffix");
        //文件夹
        if( file.isDirectory()){
            File[] files=file.listFiles();
            assert files != null;
            for(File f:files){
                doLoadFile(f);
            }
        }else{
            //是否为jsp结尾的文件
            if(file.getName().endsWith(suffix)){
                View view=new View(file.getName(),file);
                viewList.add(view);//保存文件
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) {
        //获取初始化的语言环境
        response.setHeader("content-type", "text/html;charset=utf-8");
        // 把request和response包装成ServletRequestAttributes对象
        //异步请求管理
        //统一调用doService
        doService(request, response);
    }

    private void doService(HttpServletRequest request, HttpServletResponse response) {
        // 获取当前请求的url的映射关系
        String requestURI = request.getRequestURI();
        try {
            if (!handlerMappersMap.containsKey(requestURI)) {
                response.getWriter().write("404");
                return;
            }
            HandlerMappers handlerMappers = handlerMappersMap.get(requestURI);
            /**
             * 处理当前方法请求函数
             */
            doHandleAdeapter(request, response, handlerMappers);
        } catch (Exception e) {
            e.printStackTrace();
            try {
                response.getWriter().write("error");
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    private void doHandleAdeapter(HttpServletRequest request, HttpServletResponse response, HandlerMappers handlerMappers) throws InvocationTargetException, IllegalAccessException, IOException {
        // 获取请求中所有的请求参数
        Map<String, String[]> parameterMap = request.getParameterMap();
        // 获取当前请求方法的参数信息
        List<ParameterMapper> parameterMappers = handlerMappers.getParameterMappers();
        // 获取当前请求参数中所有的参数名
        Set<String> requestParamet = parameterMap.keySet();
        // 创建请求参数数组
        Object[] params = new Object[parameterMappers.size()];
        for (int i = 0; i < parameterMappers.size(); i++) {
            ParameterMapper parameterMapper = parameterMappers.get(i);
            for (String requestParameter : requestParamet) {
                // 如果请求的参数名和当前方法中的参数名一致
                if (requestParameter.equals(parameterMapper.getParameterName())) {
                    params[i] = ParameterTypeUtils.typeConversion(parameterMapper.getParameterType(), request.getParameter(requestParameter));
                }
            }
        }
        // 开始执行目标方法的业务程序
        invokeTargetMethod(response, handlerMappers, params.length == 0 ? null:params) ;
    }

    private void invokeTargetMethod(HttpServletResponse response, HandlerMappers handlerMappers, Object[] params) throws InvocationTargetException, IllegalAccessException, IOException {
        ModelAndView modelAndView = new ModelAndView();
        // 执行目标方法
        Object invoke = handlerMappers.getMethod().invoke(handlerMappers.getTargetObj(), params);
        // 无返回值类型
        if (Objects.isNull(invoke))return;
        if (invoke instanceof ModelAndView){
            // 是一个视图的话，返回
            modelAndView = (ModelAndView) invoke;
            modelAndView.setHasView(true);
        }else{
            // 如果不是视图，返回的是Json字符串
            modelAndView.setViewData(invoke);
        }
        //把视图名称解析成对应文件名
        applyDefaultViewName(modelAndView);
        // 将结果集进行返回
        if (!modelAndView.isHasView()){
            response.getWriter().write(modelAndView.getViewData().toString());
        }
        // 是一个视图，则解析视图
       View view = getView(modelAndView.getViewData().toString());
        if (!Objects.isNull(view)){
            view.replaceAllEl(modelAndView, response);
        }
    }

    private View getView(String viewName) {
        // 解析视图文件，替换视图文件中的指定标签 EL标签
        for (View view : viewList) {
            if (view.getViewName().equals(viewName)) {
                return view;
            }
        }
        return null;
    }


    private void applyDefaultViewName(ModelAndView modelAndView) {
        if (modelAndView.isHasView()){
            String viewName = modelAndView.getViewData().toString();
            String prefix=beanFactory.getProperties("view.prefix", "");
            String suffix=beanFactory.getProperties("view.suffix", ".jsp");
            viewName=prefix+viewName+suffix;
            modelAndView.setViewData(viewName);
        }
    }

    /**
     * 获取访问控制层的方法参数，创建关系映射
     */
    private void getTargetProperties() {
        try {
            Collection<HandlerMappers> values = handlerMappersMap.values();
            for (HandlerMappers handlerMapper : values) {
                Method method = handlerMapper.getMethod();
                // 获取当前方法的参数名称列表
                List<String> parameterNameList = MethodHelper.getMethodParamNames(handlerMapper.getTargetObj().getClass(), method);
                Class<?>[] parameterTypes = handlerMapper.getMethod().getParameterTypes();
                ArrayList<ParameterMapper> parameterMappers = new ArrayList<>();
                for (int i = 0; i < parameterTypes.length; i++) {
                    parameterMappers.add(new ParameterMapper(parameterNameList.get(i), parameterTypes[i]));
                }
                handlerMapper.setParameterMappers(parameterMappers);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void getTargetController() {
        Map<String, BeanDefinition> beanDefinitionMap = context.beanFactory.beanDefinitionMap;
        Collection<BeanDefinition> beanDefinitions = beanDefinitionMap.values();
        beanDefinitions.forEach(beanDefinitio -> {
            if (beanDefinitio.getBeanClass().isAnnotationPresent(MyController.class)) {
                getTarHandlerMappers(beanDefinitio);
            }
        });
    }

    /**
     * 获取访问控制层的映射关系
     *
     * @param beanDefinitio
     */
    private void getTarHandlerMappers(BeanDefinition beanDefinitio) {
        String controllerUrl = "";
        Class<?> beanClass = beanDefinitio.getBeanClass();
        // 获取当前访问控制层全局的url开头
        if (beanClass.isAnnotationPresent(MyRequestMapping.class) && StringUtils.isNotEmpty(beanClass.getAnnotation(MyRequestMapping.class).value())) {
            String value = beanClass.getAnnotation(MyRequestMapping.class).value();
            if (!value.equals("/")) {
                if (value.startsWith("/")) {
                    controllerUrl = value;
                } else {
                    controllerUrl = "/" + value;
                }
            }
        }
        String methodUrl = "";
        // 获取当前访问控制层所有的方法
        Method[] methods = beanClass.getDeclaredMethods();
        for (Method method : methods) {
            if (method.isAnnotationPresent(MyRequestMapping.class)) {
                String value = method.getAnnotation(MyRequestMapping.class).value();
                if (StringUtils.isNotEmpty(value)) {
                    if (!value.startsWith("/")) {
                        methodUrl = controllerUrl + "/" + value;
                    } else {
                        methodUrl = controllerUrl + value;
                    }
                }
                if (StringUtils.isNotEmpty(methodUrl)) {
                    if (handlerMappersMap.containsKey(methodUrl)) {
                        throw new RuntimeException("当前" + beanClass + "的方法的请求路径与" + handlerMappersMap.get(methodUrl).getMethod() + "配置的路径一致导致冲突了");
                    }
                    handlerMappersMap.put(methodUrl, new HandlerMappers(methodUrl, context.getBean(beanDefinitio.getBeanName()), method));
                } else {
                    throw new RuntimeException("请为" + beanClass + "的方法" + method + "配置请求url");
                }
            }
        }
    }
}
