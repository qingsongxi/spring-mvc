package com.jiayao.controller;

import com.jiayao.service.GoodsInfoServiceImpl;
import context.ModelAndView;
import core.annotation.MyAutowired;
import core.annotation.MyController;
import core.annotation.MyRequestMapping;

@MyRequestMapping("goods")
@MyController
public class GoodsInfoController {

    @MyAutowired
    private GoodsInfoServiceImpl goodsInfoService;

    @MyRequestMapping("/goodsName")
    public String queryGoodsName(String goodsName, double goodsPrice){
        return "商品名称为:" + goodsName + "， 商品价值为:" + goodsPrice;
    }

    @MyRequestMapping("goodsIndex")
    public ModelAndView queryGoodsIndex(String goodsName, double goodsPrice){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewData("goodsIndex.jsp");
        modelAndView.addObject("goodsName",goodsName);
        modelAndView.addObject("goodsPrice",goodsPrice);
        return modelAndView;
    }

}
