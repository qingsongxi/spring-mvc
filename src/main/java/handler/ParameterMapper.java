package handler;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 类 名: ParameterMapper
 * 描 述:
 */
@Data
@AllArgsConstructor
public class ParameterMapper {
    /**
     * 参数名称
     */
    private String parameterName;
    /**
     * 参数类型
     */
    private Class<?> parameterType;

}
