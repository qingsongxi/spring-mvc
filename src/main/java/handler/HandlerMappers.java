package handler;

import lombok.Data;

import java.lang.reflect.Method;
import java.util.List;

@Data
public class HandlerMappers {
    /**
     * 访问的url
     */
    private String url;
    /**
     * 目标对象
     */
    private Object targetObj;
    /**
     * 目标方法
     */
    private Method method;
    /**
     * 存储方法参数
     */
    private List<ParameterMapper> parameterMappers;

    public HandlerMappers(String url, Object targetObj, Method method) {
        this.url = url;
        this.targetObj = targetObj;
        this.method = method;
    }
}
