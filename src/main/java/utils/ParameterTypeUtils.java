package utils;

/**
 * 类 名: ParameterTypeUtils
 * 描 述: 参数类型工具类
 */
public class ParameterTypeUtils {
    /**
     * @param cls 参数类型
     * @param value 参数值
     * @return
     */
    public static  Object  typeConversion(Class cls,String value){
        if(cls == Integer.class){
            return Integer.valueOf(value);
        }else if(cls == Long.class){
            return Long.parseLong(value);
        } else if(cls == String.class){
            return value;
        }else if (cls == double.class){
            return Double.valueOf(value);
        }
        return value;
    }

}
